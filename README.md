# The Dungeon Master Bot

 [![pipeline status](https://gitlab.com/ASTRELION/dungeon-master-bot/badges/main/pipeline.svg)](https://gitlab.com/ASTRELION/dungeon-master-bot/-/commits/main) 

A Mastodon bot that supplies D&D information.

https://mastodon.social/@thedungeonmaster

## Documentation

https://astrelion.gitlab.io/dungeon-master-bot/dungeon_master_bot
