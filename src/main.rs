use std::{
    fs::File,
    io::{BufRead, BufReader},
    sync::Arc,
};

use env_logger::Env;
use log::{debug, error, info, warn};
use megalodon::{
    entities::{Status, StatusVisibility},
    megalodon::PostStatusInputOptions,
    streaming::Message,
    Megalodon,
};

mod commands;
use crate::commands::CommandHandler;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>>
{
    env_logger::Builder::from_env(Env::default().default_filter_or("debug")).init();
    println!("The Dungeon Master");

    let token_file_path = "data/.token";
    let file = File::open(token_file_path);
    let file = match file
    {
        Ok(file) => file,
        Err(_e) =>
        {
            println!("Could not open {}", token_file_path);
            return Ok(());
        }
    };
    let reader = BufReader::new(file);
    let mut lines = reader.lines();
    let token = lines.next().unwrap()?;
    let url = lines.next().unwrap()?;

    println!("{:<16} {}", "Connecting to", url);
    println!(
        "{:<16} {}{}{}",
        "With token",
        &token[..3],
        "*".repeat(10),
        &token[token.len() - 3..]
    );

    let _ = start(token, url).await;

    return Ok(());
}

async fn start(token: String, url: String) -> Result<(), Box<dyn std::error::Error>>
{
    let client = Arc::new(megalodon::generator(
        megalodon::SNS::Mastodon,
        url.to_string(),
        Some(token),
        None,
    ));
    let account = client.verify_account_credentials().await?.json;
    info!("Connected as {}", account.display_name);

    let instance = client.get_instance().await?.json;

    let command_handler = Arc::new(CommandHandler::new(
        instance.clone(),
        account.clone(),
    ));

    debug!("Connecting stream {}", url);
    let streaming = client.user_streaming().await;
    streaming
        .listen(Box::new(move |message| {
            tokio::spawn(listener(
                message,
                Arc::clone(&client),
                Arc::clone(&command_handler),
            ));
        }))
        .await;

    return Ok(());
}

async fn listener(
    message: Message,
    client: Arc<Box<dyn Megalodon + Sync + Send>>,
    command_handler: Arc<CommandHandler>,
)
{
    let limit: usize = command_handler
        .instance
        .configuration
        .statuses
        .max_characters as usize;

    match message
    {
        // TODO: it may be more prudent to apply commands only on conversation
        // or notification instead of any status update.
        Message::Update(status) =>
        {
            let response = match command_handler.parse(status.clone()).await
            {
                Ok(response) => response,
                Err(err) =>
                {
                    warn!("{}", err);
                    return;
                }
            };

            info!("Received Command: {:?}", response.command);
            let at = get_at(&status);

            // Split response between multiple posts
            let mut response_str = response.reply;
            let mut responses = Vec::new();
            let response_limit = limit - 12 - at.len();
            let response_count = (response_str.len() as f32 / response_limit as f32).ceil() as i32;

            while response_str.len() > limit
            {
                let cut = response_str.split_at(response_limit);
                let str = format!(
                    "{} ({}/{})\n{}...\n",
                    at,
                    responses.len() + 1,
                    response_count,
                    cut.0,
                );
                response_str = "...".to_string() + cut.1;

                responses.push(str);
            }

            responses.push(format!(
                "{} ({}/{})\n{}\n",
                at,
                responses.len() + 1,
                response_count,
                response_str,
            ));

            // We never want to post on public timelines, so reduce to Unlisted
            let visibility = if status.visibility == StatusVisibility::Public
            {
                StatusVisibility::Unlisted
            }
            else
            {
                status.visibility
            };

            // Make the posts, chaining as needed
            let mut last_post = None;
            for response_str in responses
            {
                let mut reply_to = Some(status.id.clone());
                if last_post.is_some()
                {
                    match last_post.unwrap()
                    {
                        megalodon::megalodon::PostStatusOutput::Status(s) =>
                        {
                            reply_to = Some(s.id);
                        }
                        _ =>
                        {}
                    }
                }
                let post = client
                    .post_status(
                        response_str.clone(),
                        Some(&PostStatusInputOptions {
                            in_reply_to_id: reply_to,
                            visibility: Some(visibility.clone()),
                            ..Default::default()
                        }),
                    )
                    .await;
                last_post = post.ok().map(|p| p.json);

                match last_post
                {
                    Some(_) => info!("Replied with: {:?}", response_str),
                    None => error!("Failed to respond to command"),
                }
            }
        }
        _ =>
        {}
    }
}

fn get_at(status: &Status) -> String
{
    return "@".to_string() + &status.account.acct;
}
