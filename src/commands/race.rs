use std::collections::HashMap;

use async_trait::async_trait;
use dndrs::races::Race;

use super::{coerce_input, to_third, Command};

pub struct Cmd;

#[async_trait]
impl Command for Cmd
{
    async fn execute(
        &self,
        _base: &String,
        args: &Vec<String>,
        _commands: &HashMap<String, Box<dyn Command + Send + Sync>>,
    ) -> String
    {
        let races = dndrs::APIClient::list::<Race>().await;
        let (valid_race, args) = coerce_input(&races, &args);

        if valid_race.is_none()
        {
            return races
                .iter()
                .map(|l| l.name.clone())
                .collect::<Vec<_>>()
                .join(", ");
        }

        let race = match dndrs::APIClient::get::<Race>(valid_race.unwrap().index).await
        {
            Ok(r) => r,
            Err(_) =>
            {
                return races
                    .iter()
                    .map(|l| l.name.clone())
                    .collect::<Vec<_>>()
                    .join(", ")
            }
        };

        let emoji = match race.index.as_ref()
        {
            "dragonborn" => "🐉",
            "dwarf" => "🧔",
            "elf" => "🧝🏻",
            "gnome" => "⚙️",
            "half-elf" => "🧝🏼",
            "half-orc" => "🧌",
            "halfling" => "👣",
            "human" => "🧍",
            "tiefling" => "🧝🏾",
            _ => "",
        };

        let mut sections = Vec::from([format!("{} {}", emoji, race.name)]);

        if args.len() > 0 && args[0].starts_with("desc")
        {
            sections.append(&mut race_description(&race));
            return sections.join("\n\n");
        }

        sections.append(&mut race_stats(&race));
        return sections.join("\n\n");
    }
}

fn race_stats(race: &Race) -> Vec<String>
{
    let mut stats = Vec::new();

    let ability_bonuses = race
        .ability_bonuses
        .iter()
        .map(|a| format!("{} +{}", a.ability_score.name.clone(), a.bonus))
        .collect::<Vec<_>>()
        .join(", ");
    if ability_bonuses.len() > 0
    {
        stats.push(format!(
            "= Ability Bonuses =\n{}",
            ability_bonuses
        ));
    }

    let ability_bonus_options = race.ability_bonus_options.clone().map(|o| {
        o.from
            .options
            .iter()
            .map(|o| format!("{} +{}", o.ability_score.name, o.bonus))
            .collect::<Vec<_>>()
            .join(", ")
    });
    if ability_bonus_options.is_some()
    {
        stats.push(format!(
            "= Ability Bonus Options ({}) =\n{}",
            race.ability_bonus_options.clone().unwrap().choose,
            ability_bonus_options.unwrap()
        ));
    }

    let proficiencies = race
        .starting_proficiencies
        .iter()
        .map(|p| p.name.clone())
        .collect::<Vec<_>>()
        .join(", ");
    if proficiencies.len() > 0
    {
        stats.push(format!("= Proficiencies =\n{}", proficiencies));
    }

    let proficiency_options = race.starting_proficiency_options.clone().map(|o| {
        o.from
            .options
            .iter()
            .map(|o| o.item.name.clone())
            .collect::<Vec<_>>()
            .join(", ")
    });
    if proficiency_options.is_some()
    {
        stats.push(format!(
            "= Proficiency Options ({}) =\n{}",
            race.starting_proficiency_options.clone().unwrap().choose,
            proficiency_options.unwrap()
        ))
    }

    let languages = race
        .languages
        .iter()
        .map(|l| l.name.clone())
        .collect::<Vec<_>>()
        .join(", ");
    if languages.len() > 0
    {
        stats.push(format!("= Languages =\n{}", languages));
    }

    let language_options = race.language_options.clone().map(|o| {
        o.from
            .options
            .iter()
            .map(|o| o.item.name.clone())
            .collect::<Vec<_>>()
            .join(", ")
    });
    if language_options.is_some()
    {
        stats.push(format!(
            "= Language Options ({}) =\n{}",
            race.language_options.clone().unwrap().choose,
            language_options.unwrap()
        ));
    }

    let traits = race
        .traits
        .iter()
        .map(|t| t.name.clone())
        .collect::<Vec<_>>();
    if traits.len() > 0
    {
        stats.push(format!("= Traits =\n{}", traits.join(", ")))
    }

    stats.push(format!("= Size =\n{}", race.size));

    return stats;
}

fn race_description(race: &Race) -> Vec<String>
{
    let mut descriptions = Vec::new();

    descriptions.push(format!(
        "= Alignment =\n{}",
        to_third(&race.alignment.clone())
    ));
    descriptions.push(format!(
        "= Age =\n{}",
        to_third(&race.age.clone())
    ));
    descriptions.push(format!(
        "= Size =\n{}",
        to_third(&race.size_description.clone())
    ));
    descriptions.push(format!(
        "= Language =\n{}",
        to_third(&race.language_desc.clone())
    ));

    return descriptions;
}
