use std::collections::HashMap;

use async_trait::async_trait;

use super::Command;

pub struct Cmd;
#[async_trait]
impl Command for Cmd
{
    async fn execute(
        &self,
        _base: &String,
        _args: &Vec<String>,
        _commands: &HashMap<String, Box<dyn Command + Send + Sync>>,
    ) -> String
    {
        return "Pong!".into();
    }
}
