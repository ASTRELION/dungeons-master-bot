use std::collections::HashMap;

use async_trait::async_trait;

use super::Command;
use super::PREFIX;

pub struct Cmd;
#[async_trait]
impl Command for Cmd
{
    async fn execute(
        &self,
        _base: &String,
        _args: &Vec<String>,
        commands: &HashMap<String, Box<dyn Command + Send + Sync>>,
    ) -> String
    {
        let response = String::from("Available commands:\n");
        let command_names = commands
            .keys()
            .map(|k| PREFIX.to_owned() + &k.to_string())
            .collect::<Vec<_>>()
            .join(", ");
        return response + &command_names;
    }
}
