use std::collections::HashMap;

use async_trait::async_trait;
use dndrs::classes::Class;

use super::{coerce_input, Command};

pub struct Cmd;
#[async_trait]
impl Command for Cmd
{
    async fn execute(
        &self,
        _base: &String,
        args: &Vec<String>,
        _commands: &HashMap<String, Box<dyn Command + Send + Sync>>,
    ) -> String
    {
        let classes = dndrs::APIClient::list::<Class>().await;
        let (valid_class, args) = coerce_input(&classes, &args);

        if valid_class.is_none()
        {
            return classes
                .iter()
                .map(|l| l.name.clone())
                .collect::<Vec<_>>()
                .join(", ");
        }

        let class = match dndrs::APIClient::get::<Class>(valid_class.unwrap().index).await
        {
            Ok(c) => c,
            Err(_) =>
            {
                return classes
                    .iter()
                    .map(|l| l.name.clone())
                    .collect::<Vec<_>>()
                    .join(", ")
            }
        };

        let emoji = match class.index.as_ref()
        {
            "barbarian" => "⚒️",
            "bard" => "📯",
            "cleric" => "☀️",
            "druid" => "🌿",
            "fighter" => "⚔️",
            "monk" => "☯",
            "paladin" => "🛡️",
            "ranger" => "🏹",
            "rogue" => "🗡️",
            "sorcerer" => "💫",
            "warlock" => "🧿",
            "wizard" => "🔮",
            _ => "",
        };

        let mut sections = Vec::from([format!("{} {}", emoji, class.name)]);

        sections.append(&mut class_stats(&class));
        return sections.join("\n\n");
    }
}

fn class_stats(class: &Class) -> Vec<String>
{
    let mut stats = Vec::new();

    stats.push(format!("= Hit Die =\n{}", class.hit_die));

    let proficiency_choices = class
        .proficiency_choices
        .iter()
        .map(|p| {
            if p.desc == "Choose any three"
            {
                return "Choose any three skills".to_string();
            }

            return p.desc.clone();
        })
        .collect::<Vec<_>>();
    if proficiency_choices.len() > 0
    {
        stats.push(format!(
            "= Proficiency Choices =\n{}",
            proficiency_choices.join(", ")
        ));
    }

    let proficiencies = class
        .proficiencies
        .iter()
        .map(|c| c.name.clone())
        .collect::<Vec<_>>();
    if proficiencies.len() > 0
    {
        stats.push(format!(
            "= Proficiencies =\n{}",
            proficiencies.join(", ")
        ));
    }

    let saving_throws = class
        .saving_throws
        .iter()
        .map(|s| s.name.clone())
        .collect::<Vec<_>>();
    if saving_throws.len() > 0
    {
        stats.push(format!(
            "= Saving Throws =\n{}",
            saving_throws.join(", ")
        ));
    }

    let starting_equipment = class
        .starting_equipment
        .iter()
        .map(|e| e.equipment.name.clone())
        .collect::<Vec<_>>();
    if starting_equipment.len() > 0
    {
        stats.push(format!(
            "= Starting Equipment =\n{}",
            starting_equipment.join(", ")
        ));
    }

    let starting_equipment_options = class
        .starting_equipment_options
        .iter()
        .map(|e| e.desc.clone())
        .collect::<Vec<_>>();
    if starting_equipment_options.len() > 0
    {
        stats.push(format!(
            "= Starting Equipment Options =\n{}",
            starting_equipment_options.join("\n")
        ));
    }

    let subclasses = class
        .subclasses
        .iter()
        .map(|s| s.name.clone())
        .collect::<Vec<_>>();
    if subclasses.len() > 0
    {
        stats.push(format!(
            "= Subclasses =\n{}",
            subclasses.join(", ")
        ));
    }

    if let Some(spellcasting) = class.spellcasting.clone()
    {
        stats.push(format!(
            "= Spellcasting Ability =\n{}",
            spellcasting.spellcasting_ability.name
        ));
    }

    return stats;
}
