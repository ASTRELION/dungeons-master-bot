use std::collections::HashMap;

use async_trait::async_trait;
use dndrs::ListItem;
use html2text::render::text_renderer::TrivialDecorator;
use log::debug;
use megalodon::entities::{Account, Instance, Status};
use regex::Regex;

mod class;
mod help;
mod ping;
mod race;

pub const PREFIX: &str = "!";

#[async_trait]
pub trait Command
{
    async fn execute(
        &self,
        base: &String,
        args: &Vec<String>,
        commands: &HashMap<String, Box<dyn Command + Send + Sync>>,
    ) -> String;
}

pub struct CommandHandler
{
    pub account: Account,
    pub instance: Instance,
    pub commands: HashMap<String, Box<dyn Command + Send + Sync>>,
}

pub struct CommandResponse
{
    pub command: String,
    pub base: String,
    pub args: Vec<String>,
    pub reply: String,
}

impl CommandHandler
{
    pub fn new(instance: Instance, account: Account) -> Self
    {
        let mut s = Self {
            instance,
            account,
            commands: HashMap::new(),
        };
        s.commands.insert("class".into(), Box::new(class::Cmd));
        s.commands.insert("ping".into(), Box::new(ping::Cmd));
        s.commands.insert("help".into(), Box::new(help::Cmd));
        s.commands.insert("race".into(), Box::new(race::Cmd));
        return s;
    }

    pub async fn parse(
        &self,
        status: Status,
    ) -> Result<CommandResponse, Box<dyn std::error::Error + Send + Sync>>
    {
        if status.mentions.iter().all(|x| x.id != self.account.id)
        {
            return Err("Not a command: account not mentioned".into());
        }

        if status.account.id == self.account.id
        {
            return Err("Not a command: status is authored by bot account".into());
        }

        // Status content is a mess of HTML, so we need to reduce it to plaintext
        let content = html2text::from_read_with_decorator(
            status.content.as_bytes(),
            self.instance.configuration.statuses.max_characters as usize,
            TrivialDecorator::new(),
        )
        .trim()
        .to_string();
        debug!("Message content: '{}'", content);

        // parse plain command string from content (without prefix)
        let re = Regex::new(&format!("(@.*)\\s+{}((.)(\\s|.)*)", PREFIX)).unwrap();
        let command = re
            .captures(&content)
            .and_then(|captures| captures.get(2).map(|c| c.as_str()));

        if command == None
        {
            return Err("Not a command: command string not found".into());
        }

        // determine base command and any arguments
        let command = command.unwrap().to_lowercase();
        let split: Vec<String> = command
            .split_whitespace()
            .map(|s| s.chars().filter(|c| c.is_alphanumeric()).collect())
            .collect();
        let base = split[0].clone();
        let args = split[1..].to_vec();

        debug!("Command: {:?}", base);
        debug!("Args: {:?}", args);

        // execute command
        if let Some(exe) = self.commands.get(&base.to_string())
        {
            let reply = exe.execute(&base, &args, &self.commands).await;
            return Ok(CommandResponse {
                command,
                base,
                args,
                reply,
            });
        }

        return Err("Not a command: command not found".into());
    }
}

/// Tries to match a valid item from a `Vec` for arguments by loosely concatenating
/// arguments together, ignoring special characters. If a match is found, then a
/// `Some` of the matched item is returned. In addition, if arguments were
/// successfully used to match an item, then a new `Vec` of arguments without the
/// consumed arguments will be returned.
fn coerce_input(
    possible_items: &Vec<ListItem>,
    args: &Vec<String>,
) -> (Option<ListItem>, Vec<String>)
{
    if args.len() == 0
    {
        return (None, args.clone());
    }

    let mut valid_item: Option<ListItem> = None;
    let mut args_used = 0;
    for race in possible_items
    {
        let mut index: String = race.index.chars().filter(|c| c.is_alphanumeric()).collect();
        let mut a = 0;
        for arg in args
        {
            if index.starts_with(arg)
            {
                index = index.replacen(arg, "", 1);
                a += 1;
            }
            else
            {
                break;
            }
        }

        // the entire index has been matched
        if index.len() == 0
        {
            valid_item = Some(race.clone());
            args_used = a;
            break;
        }
    }

    let args = args[args_used..].to_vec();
    return (valid_item, args);
}

/// Convert a string with "your" and "you" to "their" and "they"
fn to_third(str: &String) -> String
{
    return str
        .replace("your", "their")
        .replace("Your", "Their")
        .replace("you", "they")
        .replace("You", "They");
}
