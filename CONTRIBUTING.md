# Contributing

## Development

This repository uses unstable formatting rules defined in [`rustfmt.toml`](./rustfmt.toml), so you should use use the [nightly channel of Rust](https://rust-lang.github.io/rustup/concepts/channels.html#working-with-nightly-rust) and then enable autoformatting in your editor.

### Run

```
cargo run
```

### Build

```
cargo build [--release]
```

### Generate Docs

```
cargo doc --no-deps
```
