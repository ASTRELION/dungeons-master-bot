FROM rust:1-alpine as build

RUN apk update && apk upgrade
RUN apk add openssl-dev musl-dev

COPY src/ src/
COPY Cargo.toml Cargo.toml
COPY Cargo.lock Cargo.lock
ENV RUSTFLAGS="-C target-feature=-crt-static"
RUN cargo build --release

FROM rust:1-alpine

RUN apk update && apk upgrade
COPY --from=build ./target/release/dungeon-master-bot .

CMD [ "./dungeon-master-bot" ]
